package stamper.mvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import stamper.mvp.presenter.MainPresenter;

/**
 * @author Corentin Stamper
 */
public class MainActivity extends AppCompatActivity {

    private MainPresenter presenter;

    private EditText firstname, lastname;
    private Button update;
    private TextView user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.presenter = new MainPresenter(this);

        this.firstname = (EditText) findViewById(R.id.firstname);
        this.lastname = (EditText) findViewById(R.id.lastname);
        this.update = (Button) findViewById(R.id.updateButton);
        this.user = (TextView) findViewById(R.id.userDescription);

        this.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstname = MainActivity.this.firstname.getText().toString();
                String lastname = MainActivity.this.lastname.getText().toString();
                presenter.updateUser(firstname, lastname);
            }
        });
    }

    public void updateUserDescription(String userDescription) {
        this.user.setText(userDescription);
    }
}
