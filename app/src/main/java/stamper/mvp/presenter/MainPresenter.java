package stamper.mvp.presenter;

import android.view.View;

import stamper.mvp.MainActivity;
import stamper.mvp.model.User;

/**
 * @author Corentin Stamper
 */
public class MainPresenter {

    private User model;
    private MainActivity view;

    public MainPresenter(MainActivity view) {
        this.view = view;
        this.model = new User();
    }

    public void updateUser(String firstname, String lastname) {
        boolean first = updateFirstname(firstname);
        boolean last = updateLastname(lastname);

        if (first || last) {
            view.updateUserDescription(model.toString());
        }
    }

    private boolean updateFirstname(String firstname) {
        if (model.getFirstname().equals(firstname)) {
            return false;
        }

        model.setFirstname(firstname);
        return true;
    }

    private boolean updateLastname(String lastname) {
        if (model.getLastname().equals(lastname)) {
            return false;
        }

        model.setLastname(lastname);
        return true;
    }
}
